import DS from 'ember-data';
const {attr, belongsTo, Model} = DS;

export default Model.extend({
  tool: belongsTo('tool'),
  startTime: attr('date'),
  endTime: attr('date'),
  userId: attr('string'),
  userName: attr('string'),
  email: attr('string'),
  phoneNumber: attr('string'),
  description: attr('string')

});

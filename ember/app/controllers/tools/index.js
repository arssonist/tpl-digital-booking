import Ember from 'ember';
import moment from 'moment';

export default Ember.Controller.extend({
  queryParams: ['type'],
  type: null,

  formattedType: Ember.computed('type', function() {
    // TODO: actual i18n or something
    let type = this.get('type');
    if(type === '3dprinter') {
      return '3D Printers';
    } else {
      return type;
    }
  }),

  today: Ember.computed(function() {
    return moment();
  }).readOnly(),

  date: Ember.computed(function() {
    return moment();
  }),

  dateSelections: Ember.computed(function() {
    let offsets = [0, 1, 2, 3, 4];
    let selections = offsets.map((offset) => {
      return moment().add(offset, 'days');
    });

    return selections;
  }),

  actions: {
    setDate(newDate) {
      this.set('date', newDate);
    }
  }
});

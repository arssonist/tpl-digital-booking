import Ember from 'ember';
import moment from 'moment';

export default Ember.Component.extend({
  classNames: ['col-xs-2', 'text-center', 'well', 'well-lg'],
  classNameBindings: ['isSelected:selected'],

  isSelected: Ember.computed('selection', 'selectedDate', function() {
    let selection = this.get('selection');
    let selectedDate = this.get('selectedDate');

    return moment(selection).format('YYYYMMDD') ===
      moment(selectedDate).format('YYYYMMDD');
  }),

  click() {
    this.get('onClick')();
  }
});

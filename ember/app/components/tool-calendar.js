import Ember from 'ember';

// temp until we figure out the per-tool scheduling
const OPEN_HOUR = 9;
const CLOSE_HOUR = 20;

export default Ember.Component.extend({
  tagName: 'article',
  classNames: ['tool-calendar'],

  timeslots: Ember.computed('date', function() {
    let slots = [];

    let date = this.get('date');

    for(let hour = OPEN_HOUR; hour < CLOSE_HOUR; hour++) {
      slots.pushObject({
        time: date.clone().set({
          hour,
          minute: 0,
          second: 0,
          millisecond: 0
        })
      });
    }

    return slots;
  })
});

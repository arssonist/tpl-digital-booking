import Ember from 'ember';
import moment from 'moment';

const BOOKING_LENGTH_IN_HOURS = 2;

export default Ember.Controller.extend({
  queryParams: ['startTime'],
  startTime: null,

  endTime: Ember.computed('startTime', function() {
    let startTime = this.get('startTime');
    if(Ember.isPresent(startTime)) {
      return moment(startTime).add(BOOKING_LENGTH_IN_HOURS, 'hours');
    } else {
      return null;
    }
  }),

  actions: {
    submit() {
      // TODO: get this working!

      // let controllerProps = this.getProperties([
      //   'tool',
      //   'startTime',
      //   'endTime'
      // ]);

      // let model = this.get('model');

      // model.setProperties(controllerProps);

      // return model.save().then(() => {
      //   alert('Done!');
      // }).catch(() => {
      //   alert('Error!');
      // });

      this.transitionToRoute('bookings.show', 1);
    }
  }
});

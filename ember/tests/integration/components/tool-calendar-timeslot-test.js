import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('tool-calendar-timeslot', 'Integration | Component | tool calendar timeslot', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{tool-calendar-timeslot}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#tool-calendar-timeslot}}
      template block text
    {{/tool-calendar-timeslot}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});

import Ember from 'ember';
import moment from 'moment';

export default Ember.Component.extend({
  timeFormat: 'h:mm A',

  isoFormatTime: Ember.computed('timeslot.time', function() {
    return moment(this.get('timeslot.time')).format();
  })
});

import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    return Ember.RSVP.hash({
      booking: this.store.createRecord('booking'),
      tool: this.store.find('tool', params.toolId)
    });
  },

  setupController(controller, resolvedModel) {
    controller.set('tool', resolvedModel.tool);
    return this._super(controller, resolvedModel.booking);
  }
});

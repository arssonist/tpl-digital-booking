# DigitalBooking

This project is an online digital equipment reservation system for the Toronto Public Library. The initial version of this project came together over ~30 hours during a weekend hackathon at the Reference Library, ProsperityHack.

This repository contains two separate applications:

* __ember__ – an Ember-CLI project which consumes data retrieved from a JSON API
* __java__ – Java servlet(s) which interface with a database and provide a JSON API

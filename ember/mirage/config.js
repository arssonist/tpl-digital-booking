const TOOLS = [
  {
    id: 1,
    branchId: 1,
    name: 'TRL 3D Printer 1 - Makerbot Replicator 2',
    type: '3dprinter',
    requirements: [
      'GOOD_STANDING',
      '3D_PRINTER'
    ],
    description: 'This is the 3D printer at the Toronto Reference Library'
  },
  {
    id: 2,
    branchId: 1,
    name: 'TRL 3D Printer 2 - Makerbot Replicator 2',
    type: '3dprinter',
    requirements: [
      'GOOD_STANDING',
      '3D_PRINTER'
    ],
    description: 'This is the other 3D printer at the Toronto Reference Library'
  },
  {
    id: 3,
    branchId: 1,
    name: 'TRL 3D Printer 3 - Lulzbot Taz',
    type: '3dprinter',
    requirements: [
      'GOOD_STANDING',
      '3D_PRINTER'
    ],
    description: 'This is the other 3D printer at the Toronto Reference Library'
  },
  {
    id: 4,
    branchId: 2,
    name: 'Fort York 3D Printer',
    type: '3dprinter',
    requirements: [
      'GOOD_STANDING',
      '3D_PRINTER'
    ],
    description: 'This is the 3D printer at the Fort York branch'
  },
  {
    id: 5,
    branchId: 3,
    name: 'Scarborough 3D Printer',
    type: '3dprinter',
    requirements: [
      'GOOD_STANDING',
      '3D_PRINTER'
    ],
    description: 'This is the other 3D printer at the Scarborough Civic Centre branch'
  }
];

export default function() {

  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).

    Note: these only affect routes defined *after* them!
  */

  // this.urlPrefix = '';    // make this `http://localhost:8080`, for example, if your API is on a different server
  // this.namespace = '';    // make this `api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  /*
    Shorthand cheatsheet:

    this.get('/posts');
    this.post('/posts');
    this.get('/posts/:id');
    this.put('/posts/:id'); // or this.patch
    this.del('/posts/:id');

    http://www.ember-cli-mirage.com/docs/v0.2.x/shorthands/
  */

  this.get('/tools', () => {
    return {
      tools: TOOLS
    };
  });

  this.get('/tools/:id', (schema, request) => {
    let tool = TOOLS[request.params.id - 1];
    return {
      tool: tool
    };
  });

  this.post('/bookings', (schema, request) => {
    var params = JSON.parse(request.requestBody);

    // if (!params.title) {
    //   return new Mirage.Response(422, {some: 'header'}, {errors: {title: ['cannot be blank']}});
    // } else {
    return schema.bookings.create(params);
    // }
  });

  this.get('/bookings/:id', (schema, request) => {
    return {
      booking: {
        id: request.params.id,
        toolId: 1,
        startTime: '2016-09-17T09:00:00-04:00',
        endTime: '2016-09-17T11:00:00-04:00',
        userName: 'Page N. Panel',
        email: 'nospamplz@example.com',
        phoneNumber: '416-555-0123',
        description: 'I wanna use the 3D printer'
      }
    };
  });
}
